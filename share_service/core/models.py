from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.translation import gettext_lazy as _
from stdimage import StdImageField


class CustomUserManager(UserManager):
    def create_superuser(self, email, password=None, **extra_fields):
        super().create_superuser(username=email, email=email, password=password)


class User(AbstractUser):
    email = models.EmailField(_('email_address'), blank=True, unique=True)
    is_verify = models.BooleanField(default=False)
    avatar = StdImageField(
        upload_to="avatars", blank=True, null=True, variations={"thumbnail": {"width": 100, "height": 55}}
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def save(self, *args, **kwargs):
        if not self.pk:
            self.username = self.email
        super(User, self).save(*args, **kwargs)

    def __str__(self):
        return self.email
