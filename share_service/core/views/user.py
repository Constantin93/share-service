from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet, ViewSet
from rest_framework.mixins import ListModelMixin
from drf_yasg.utils import swagger_auto_schema

from core.models import User
from core.serializers.user import UserSerializer


class UserViewset(GenericViewSet, ListModelMixin):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_current_user(self):
        return self.request.user

    @swagger_auto_schema(
        operation_id="user_read", responses={200: UserSerializer()}
    )
    def list(self, request, *args, **kwargs):
        return Response(self.serializer_class(self.get_current_user()).data)
