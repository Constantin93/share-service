# Share Service

### Install poetry
```shell
pip install poetry
```

### Build image, create and start database container
```shell
docker-compose up -d --build
```

### Spawn a shell within the virtual environment
```shell
poetry shell
```

### Run Celery
```shell
celery -A share_service worker -l INFO
```

### Run flower
```shell
flower -A share_service --port=5555
```