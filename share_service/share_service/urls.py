from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from drf_yasg.generators import OpenAPISchemaGenerator
from rest_framework.permissions import IsAuthenticated

# schema_view = get_schema_view(
#     openapi.Info(title="Sharing Service API", default_version="v0.1"),
#     public=True,
#     generator_class=OpenAPISchemaGenerator,
#     permission_classes=(IsAuthenticated, )
# )

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include("core.urls", namespace="api")),
]

