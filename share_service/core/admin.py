from django.contrib import admin

from core.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ("id",
                    "username",
                    "email",
                    "first_name",
                    "last_name",
                    "is_verify",
                    "avatar"
                    )
    list_display_links = ("id", "username")
    search_fields = ("email",)


admin.site.register(User, UserAdmin)
