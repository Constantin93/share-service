from django.urls import path, include
from rest_framework.routers import SimpleRouter
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.views import get_schema_view
from drf_yasg.utils import swagger_auto_schema

from core.views.user import UserViewset

app_name = "core"

router = SimpleRouter()
router.register("user", UserViewset, basename="user")

schema_view = get_schema_view(
    openapi.Info(title="Sharing Service API", default_version="v0.1"),
    public=True,
    generator_class=OpenAPISchemaGenerator,
    permission_classes=(IsAuthenticated, )
)

urlpatterns = [
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui",),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]

urlpatterns += router.urls
