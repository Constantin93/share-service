import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "share_service.settings")

app = Celery("share_service")  # Экзампляр селери

app.config_from_object("django.conf:settings", namespace="CELERY")

app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Hello world!')
